// solution1
var addDigits = function (num) {
  const getDigits = (n) => {
    if (n < 10) return n;

    const nSplit = String(n).split('');
    const sum = nSplit.reduce((total, currN) => {
      return total += Number(currN)
    }, 0)

    return getDigits(sum)
  }

  return getDigits(num);
};

// solution2
var addDigits = function (num) {
  if (num == 0) return 0;
  const remain = num % 9;
  return remain === 0 ? 9 : remain
};


console.log(addDigits(38))