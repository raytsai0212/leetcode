var isUgly = function (n) {
  if (n === 0) return false;
  if (n === 1) return true;
  if ([2, 3, 5].indexOf(n) > -1) return true;

  const canDivideBy235 = (num) => {
    if (num === 1) {
      return true;
    }

    if (num % 2 === 0) {
      return canDivideBy235(num / 2)
    }

    if (num % 3 === 0) {
      return canDivideBy235(num / 3)
    }

    if (num % 5 === 0) {
      return canDivideBy235(num / 5)
    }

    return false;
  }

  return canDivideBy235(n)
};


console.log(isUgly(9));
// console.log(isUgly(6));
// console.log(isUgly(1));
// console.log(isUgly(14));