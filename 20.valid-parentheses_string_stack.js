/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function (s) {
  if (s.length % 2 === 1) {
    return false;
  }

  const hashtable = {
    '(': ')',
    '[': ']',
    '{': '}',
  }

  const opening = Object.keys(hashtable);
  let stack = [];

  for (let i = 0; i < s.length; i++) {

    if (opening.includes(s[i])) {
      stack.push(s[i]);
    } else {
      // 這裡不寫也可
      if(stack.length === 0){
        return false;
      }

      const last = stack.pop();
      if (s[i] !== hashtable[last]) {
        return false;
      }
    }
  }

  return stack.length === 0;
};


// console.log('()', isValid('()'))
// console.log('()[]{}', isValid('()[]{}'))
// console.log('(]', isValid('(]'))
// console.log('([])', isValid('([])'))
// console.log('((', isValid('(('))