var containsDuplicate = function (nums) {
  const mapping = {};

  for (let i = 0; i < nums.length; i++) {
    if (!mapping[nums[i]]) {
      mapping[nums[i]] = true;
    } else {
      return true;
    }
  }

  return false;
};
