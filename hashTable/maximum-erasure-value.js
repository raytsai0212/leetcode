/**
 * @param {number[]} nums
 * @return {number}
 */

// approach 1
var maximumUniqueSubarray = function (nums) {

  const arr = [];
  let maxSum = 0;


  for (let i = 0; i < nums.length; i++) {

    while (arr.includes(nums[i])) {
      arr.shift();
    }
    arr.push(nums[i]);

    const sum = arr.reduce((total, ele) => {
      total += ele;
      return total
    }, 0)

    maxSum = Math.max(maxSum, sum);
  }

  return maxSum;
};

// approach2

var maximumUniqueSubarray2 = function (nums) {

  const arr = [];
  let maxSum = 0;
  let tempSum = 0;

  for (let i = 0; i < nums.length; i++) {
      while (arr.includes(nums[i])) {
          const firstEle = arr.shift();
          tempSum -= firstEle;
      }
      arr.push(nums[i]);

      tempSum += nums[i]
      maxSum = Math.max(maxSum, tempSum);
  }

  return maxSum;
};
