// solution1
var majorityElement = function (nums) {
  const countObj = nums.reduce((obj, item) => {
    if (!obj[item]) {
      obj[item] = 0;
    }

    obj[item] += 1
    return obj
  }, {})


  let max = 0;
  let num = 0;
  for (let key in countObj) {
    if (countObj[key] > max) {
      max = countObj[key];
      num = Number(key)
    }
  }

  return num;
};

// solution2 (faster)
var majorityElement2 = function (nums) {
  if (nums.length === 1) return nums[0]
  const countObj = {};

  const half = nums.length / 2
  for (let i = 0; i < nums.length; i++) {
    if (!countObj[nums[i]]) {
      countObj[nums[i]] = 1;
    } else {
      countObj[nums[i]] += 1;

      if (countObj[nums[i]] > half) {
        return nums[i]
      }
    }
  }
};

console.log(majorityElement2([3, 2, 3]))