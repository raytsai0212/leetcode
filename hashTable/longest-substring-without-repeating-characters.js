/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function (s) {
  if (s.length <= 1) {
    return s.length;
  }

  let maxLen = 0;
  let str = '';

  for (let i = 0; i < s.length; i++) {
    while (str.includes(s[i])) {
      str = str.slice(1, i);
    }
    
    str += s[i];
    maxLen = Math.max(maxLen, str.length);
  }

  return maxLen;
};

console.log(lengthOfLongestSubstring("abcabcbb"));
console.log(lengthOfLongestSubstring("pwwkew"));
console.log(lengthOfLongestSubstring("bbbbb"));
console.log(lengthOfLongestSubstring("au"));


// 1. 一次推一個字母進去
// 2. 若