// solution1 O(n2)
var missingNumber = function (nums) {
  for (let i = 0; i <= nums.length; i++) {
    if (nums.indexOf(i) === -1) {
      return i
    }
  }

  return 0
};

// solution2 O(1)
var missingNumber = function (nums) {
  let total = (1 = nums.length) / 2 * nums.length;
  let numsTotal = 0
  for (let i = 0; i < nums.length; i++) {
    numsTotal += nums[i]
  }

  return total - numsTotal
};


// solution3 O(1) XOR (js 中使用位元運算子不會比較快，solution2 速度最快)
var missingNumber = function (nums) {
  let res = 0, i;
  for (i = 0; i < nums.size(); i++) {
    res = res ^ nums[i] ^ i;
  }
  return res ^ i;
};


console.log(missingNumber([0, 1]))