// O(n)
var maxProfit = function (prices) {
  let min = Number.MAX_SAFE_INTEGER;
  let profit = 0;

  for (let i = 0; i < prices.length; i++) {
    if (prices[i] < min) {
      min = prices[i]
    }

    const currProfit = prices[i] - min;

    if (currProfit > 0) {
      profit += currProfit;
      min = Number.MAX_SAFE_INTEGER;
      if (prices[i] < min) {
        min = prices[i]
      }
    }
  }

  return profit;
};

console.log(maxProfit([7, 1, 5, 3, 6, 4]));
// console.log(maxProfit([7, 6, 4, 3, 1]));
// console.log(maxProfit([2, 4, 1]));
// console.log(maxProfit([7, 6, 4, 3, 1]));
// console.log(maxProfit([1, 2]))
// console.log(maxProfit([2, 1]))
