
var findWords = function (words) {
  const keyboardRows = ["qwertyuiop", "asdfghjkl", "zxcvbnm"];

  const mapping = keyboardRows.reduce((obj, row, idx) => {
      const rowLetters = row.split('');

      rowLetters.forEach(letter => {
          obj[letter] = idx
      })

      return obj
  }, {})

  const matchWordAndKeyboardRow = (wordList, firstCode) => {
      for (let i = 0; i < wordList.length; i += 1) {
          if (firstCode !== mapping[wordList[i]]) {
              return false;
          }
      }

      return true;
  }

  const results = []
  words.forEach((word) => {
      const wordList = word.toLowerCase().split('');
      const firstCode = mapping[wordList[0]];
      const isAvailable = matchWordAndKeyboardRow(wordList, firstCode);

      if (isAvailable) {
          results.push(word)
      }
  })

  return results
};