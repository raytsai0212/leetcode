var generate = function (rowIndex) {
  const arr = [];

  for (i = 0; i < rowIndex + 1; i += 1) {
    const temp = []
    for (j = 0; j <= i; j += 1) {
      if (j === 0 || j === i) {
        temp.push(1)
      } else {
        temp.push(arr[i - 1][j - 1] + arr[i - 1][j])
      }
    }

    arr.push(temp)
  }

  return arr[rowIndex];
};

console.log(generate(3))