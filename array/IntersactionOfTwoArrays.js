var intersection = function (nums1, nums2) {
  const count = {};
  for (let i = 0; i < nums1.length; i++) {
    if (!count[nums1[i]]) {
      count[nums1[i]] = 1;
    }
  }

  let record = [];
  for (let i = 0; i < nums2.length; i++) {
    if (nums2[i] in count) {
      record.push(nums2[i])
    }
  }

  record = record.filter((n, i, arr) => arr.indexOf(n) === i);

  return record;
};