// 可以跑，效能很差
var intersect = function (nums1, nums2) {
  const count = {};
  for (let i = 0; i < nums1.length; i++) {
    if (!count[nums1[i]]) {
      count[nums1[i]] = [0, 0];
    }

    count[nums1[i]][0] += 1;
  }


  for (let i = 0; i < nums2.length; i++) {
    if (nums2[i] in count) {
      count[nums2[i]][1] += 1;
    }
  }

  const keys = Object.keys(count);
  let record = [];
  for (let i = 0; i < keys.length; i++) {
    console.log(count[keys[i]])

    // const keyCount = count[keys[i]][1] - count[keys[i]][0];
    const min = Math.min(count[keys[i]][1], count[keys[i]][0]);
    if (count[keys[i]][1] !== 0 && count[keys[i]][0] !== 0) {
      for (let k = 0; k < min; k++) {
        record.push(keys[i])
      }
    }
  }


  return record;
};

// console.log(intersect([4, 9, 5], [9, 4, 9, 8, 4]))
console.log(intersect([1, 2, 2, 1], [2, 2]))