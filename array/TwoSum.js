// var twoSum = function (nums, target) {
//   let tempIndex = 0;
//   for (let i = 0; i < nums.length; i += 1) {

//     tempIndex = i;
//     const findNum = target - nums[i];
//     const matchIndex = nums.findIndex((n, idx) => n === findNum & i !== idx);

//     console.log(matchIndex)

//     if (matchIndex > 0) {
//       return [tempIndex, matchIndex]
//     }
//   }
// };

// console.log(twoSum([3, 3], 6))

// var twoSumByHashMap = function (nums, target) {
//   const hashmap = {};

//   for (let i = 0; i < nums.length; i += 1) {
//     hashmap[nums[i]] = i
//   }

//   for (let i = 0; i < nums.length; i += 1) {
//     const goal = target - nums[i];
//     if (goal in hashmap && i !== hashmap[goal]) {
//       return [i, hashmap[goal]]
//     }
//   }
// };

var twoSum = function(nums, target) {

  const hashMap = {}
  for (let i = 0; i < nums.length; i += 1) {
      const remainNum = target - nums[i];

      if(remainNum in hashMap) {
          return [hashMap[remainNum], i]
      }
      hashMap[nums[i]] = i
  }
};

console.log(twoSum([2, 7, 11, 16], 9))