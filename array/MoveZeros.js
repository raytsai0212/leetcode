// 沒有問題，但超大陣列時，會跑很久
// var moveZeroes = function (nums) {
//   if (nums.length === 0 || nums.length === 1) return nums;

//   for (let i = 0; i < nums.length; i++) {
//     if (nums[i] !== 0) {
//       let currIndex = i;
//       console.log('currIndex', currIndex)
//       while (currIndex > 0 && nums[currIndex - 1] === 0) {
//         const temp = nums[currIndex - 1];
//         nums[currIndex - 1] = nums[currIndex];
//         nums[currIndex] = temp;

//         currIndex -= 1;

//       };
//       console.log('nums', nums)
//     }
//   }
// };


var moveZeroes = function (nums) {
  let index = 0;

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] !== 0) {
      nums[index++] = nums[i]
    }
  }

  for (let i = index; i < nums.length; i++) {
    nums[i] = 0;
  }
}


moveZeroes([0, 1, 0, 3, 12])
// moveZeroes([1, 0])