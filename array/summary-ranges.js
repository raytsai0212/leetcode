var summaryRanges = function (nums) {
  if (nums.length === 0) {
    return nums;
  }
  let arr = [];
  arr.push([nums[0]]);

  for (let i = 1; i < nums.length; i++) {
    if (nums[i - 1] + 1 === nums[i]) {
      arr[arr.length -1].push(nums[i])
    } else {
      arr.push([nums[i]]);
    }
  }

  arr = arr.map(item => {
    if(item.length === 1){
      return item[0]
    }

    return `${item[0]}->${item[item.length -1]}`
  })

  return arr;
};


console.log(summaryRanges([0, 1, 2, 4, 5, 7]))