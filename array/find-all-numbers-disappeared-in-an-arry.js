var findDisappearedNumbers = function (nums) {
  let arr = Array(nums.length).fill(undefined);
  const result = [];

  for (let i = 0; i < nums.length; i += 1) {
      arr[nums[i]] = true;
  }
  
  for (let i = 0; i < nums.length; i += 1) {
      if (!arr[i+1]) {
        result.push(i + 1);
      }
  }

  return result
};

console.log(findDisappearedNumbers([4,3,2,7,8,2,3,1]))

