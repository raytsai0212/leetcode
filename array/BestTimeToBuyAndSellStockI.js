// O(n^2) 沒有問題，但超大陣列時，會跑很久 
// var maxProfit = function (prices) {
//   if (prices.length === 0 || prices.length === 1) return 0

//   let max = 0;
//   for (let i = 0; i < prices.length; i++) {
//     for (let k = i + 1; k < prices.length; k++) {
//       if (prices[k] > prices[i]) {
//         const tempMax = prices[k] - prices[i];
//         if (tempMax > max) {
//           max = tempMax;
//         }
//       }
//     }
//   }

//   return max;
// };

// O(n)
var maxProfit = function (prices) {
  let min = Number.MAX_SAFE_INTEGER;
  let profit = 0;

  for (let i = 0; i < prices.length; i++) {
    if (prices[i] < min) {
      min = prices[i]
    }

    const currProfit = prices[i] - min;

    if (currProfit > profit) {
      profit = currProfit
    }
  }

  return profit;
};

console.log(maxProfit([7, 1, 5, 3, 6, 4]));
console.log(maxProfit([7, 6, 4, 3, 1]));
console.log(maxProfit([2, 4, 1]));
console.log(maxProfit([7, 6, 4, 3, 1]));
console.log(maxProfit([1, 2]))
console.log(maxProfit([2, 1]))
