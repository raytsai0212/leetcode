// 可以用，但效能頗差
var findShortestSubArray = function (nums) {
  const count = {}
  for (let i = 0; i < nums.length; i++) {
    if (!count[nums[i]]) {
      count[nums[i]] = 0;
    }
    count[nums[i]] += 1;
  }

  const maxNum = Math.max(...Object.values(count));
  const keys = Object.keys(count);
  const matchNums = keys.reduce((list, key) => {
    if (count[key] === maxNum) {
      list.push(Number(key))
    }
    return list
  }, [])

  const recordObj = {}

  for (let i = 0; i < nums.length; i++) {
    console.log(nums[i], '-', maxNum)
    if (matchNums.includes(nums[i])) {
      // console.log('h')
      console.log(nums[i])
      if (!recordObj[nums[i]]) {
        recordObj[nums[i]] = [];
      }

      recordObj[nums[i]].push(i);
    }
  }


  let minLength = Number.MAX_SAFE_INTEGER;
  const recordValues = Object.values(recordObj);

  for (let i = 0; i < recordValues.length; i++) {
    const diff = recordValues[i][recordValues[i].length - 1] - recordValues[i][0] + 1;
    if (diff < minLength) {
      minLength = diff;
    }
  }

  return minLength

};



console.log(findShortestSubArray([1, 2, 2, 3, 1]));
console.log(findShortestSubArray([1, 2, 2, 3, 1, 4, 2]));