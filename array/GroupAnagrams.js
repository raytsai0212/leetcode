var groupAnagrams = function(strs) {

  const result = strs.reduce((obj, str) => {
      const sortedStr = str.split('').sort().join('');

      if(!obj[sortedStr]) {
          obj[sortedStr] = []
      }

      obj[sortedStr].push(str);

      return obj
  }, {})

  return Object.values(result)
};