/**
 * @param {number[][]} matrix
 * @return {void} Do not return anything, modify matrix in-place instead.
 */
var rotate = function (matrix) {
  const matrixLength = matrix.length
  let beginIdx = 0
  let endIdx = matrixLength - 1

  while (beginIdx < endIdx) {
      for (let i = 0; i < matrixLength; i++) {
          const temp = matrix[beginIdx][i]
          matrix[beginIdx][i] = matrix[endIdx][i]
          matrix[endIdx][i] = temp;
      }

      beginIdx += 1;
      endIdx -= 1;
  }

  for (let i = 0; i < matrixLength; i += 1) {
      for (let j = i + 1; j < matrixLength; j += 1) {
          const temp = matrix[i][j]
          matrix[i][j] = matrix[j][i]
          matrix[j][i] = temp;
      }
  }
};


console.log(rotate([[1,2,3],[4,5,6],[7,8,9]]))