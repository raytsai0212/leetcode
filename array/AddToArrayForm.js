var addToArrayFormByReverse = function (num, k) {
  const kNum = String(k).split('').map(i => Number(i)).reverse();
  const nNum = num.reverse();
  const diff = nNum.length - kNum.length;
  const postiveDiff = Math.abs(diff);

  if (diff > 0) {

    for (let i = 0; i < postiveDiff; i += 1) {
      kNum.push(0)
    }
  }

  if (diff < 0) {
    for (let i = 0; i < postiveDiff; i += 1) {
      nNum.push(0)
    }
  }

  let arr = Array(nNum.length + 1).fill(0);
  for (let i = 0; i < nNum.length; i += 1) {
    const n = kNum[i] + nNum[i] + (arr[i] || 0);
    const unit = n % 10;
    let add = n >= 10;
    arr[i] = unit;
    arr[i + 1] = add ? arr[i + 1] + 1 : arr[i + 1];
  }

  if (!arr[arr.length - 1]) {
    arr.pop()
  }

  return arr.reverse();
};


var addToArrayForm = function (num, k) {
  const kNum = String(k).split('').map(i => Number(i));
  const nNum = num;
  const diff = nNum.length - kNum.length;
  const postiveDiff = Math.abs(diff);

  if (diff > 0) {

    for (let i = 0; i < postiveDiff; i += 1) {
      kNum.splice(0, 0, 0)
    }
  }

  if (diff < 0) {
    for (let i = 0; i < postiveDiff; i += 1) {
      nNum.splice(0, 0, 0)
    }
  }

  let arr = Array(nNum.length).fill(0);
  for (let i = nNum.length - 1; i > -1; i -= 1) {
    const n = kNum[i] + nNum[i] + arr[i];
    const unit = n % 10;
    let add = n >= 10;
    arr[i] = unit;
    if (add) {
      if (i - 1 === -1) {
        arr.splice(0, 0, 1);
      } else {
        arr[i - 1] += 1;
      }
    }
  }
  return arr;
};

console.log(addToArrayForm([1, 2, 0, 0], 34))
console.log(addToArrayForm([1, 2, 6, 3, 0, 7, 1, 7, 1, 9, 7, 5, 6, 6, 4, 4, 0, 0, 6, 3], 516))
console.log(addToArrayForm([2, 7, 4], 181))
console.log(addToArrayForm([2, 1, 5], 806))

