const checkUnique = (row) => {
  const originRow = row.filter(item => item !== '.');
  const resultRow = originRow.filter((r, idx, arr) => arr.indexOf(r) === idx)

  if (originRow.length !== resultRow.length) {
      return false;
  }

  return true;
}

var isValidSudoku = function (board) {
  // step 1. valid each row without repetition
  // step 2. valid each column without repetition  
  for (let i = 0; i < 9; i += 1) {
      const row = []
      const col = []
      for (let j = 0; j < 9; j += 1) {
          row.push(board[i][j])
          col.push(board[j][i])
      }

      const colResult = checkUnique(col)
      const rowResult = checkUnique(row)

      if (!rowResult || !colResult) {
          return false
      }
  }

  // step 3. valide each 3 x 3 box without repetition
  for (let i = 0; i <= 6; i += 3) {
      for (let j = 0; j <= 6; j += 3) {
          const row = []
          for (let k = 0; k < 3; k += 1) {
              for (let h = 0; h < 3; h += 1) {
                  row.push(board[i + k][j + h])
              }


              const result = checkUnique(row)

              if (!result) {
                  return false
              }
          }
      }
  }

  return true;
};

console.log(isValidSudoku([
  ["5", "3", ".", ".", "7", ".", ".", ".", "."],
  ["6", ".", ".", "1", "9", "5", ".", ".", "."],
  [".", "9", "8", ".", ".", ".", ".", "6", "."],
  ["8", ".", ".", ".", "6", ".", ".", ".", "3"],
  ["4", ".", ".", "8", ".", "3", ".", ".", "1"],
  ["7", ".", ".", ".", "2", ".", ".", ".", "6"],
  [".", "6", ".", ".", ".", ".", "2", "8", "."],
  [".", ".", ".", "4", "1", "9", ".", ".", "5"],
  [".", ".", ".", ".", "8", ".", ".", "7", "9"]
]))

