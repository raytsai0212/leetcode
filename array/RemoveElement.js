// var removeElement = function (nums, val) {
//   if (nums.length === 0) return 0;
//   if (nums.length === 1 && nums[0] === val) {
//     nums.pop()
//     return 1;
//   }
//   if (!nums.includes(val)) return nums.length;

//   let headIndex = 0;
//   let tailIndex = nums.length - 1;

//   while (tailIndex > headIndex) {
//     if (nums[headIndex] !== val) {
//       headIndex += 1;
//     } else {
//       const temp = nums[headIndex];
//       nums[headIndex] = nums[tailIndex];
//       nums[tailIndex] = temp;
//       tailIndex -= 1
//     }
//   }
//   return headIndex + 1
// };

var removeElement = function (nums, val) {
  if (nums.length == 0) return nums.length;
  if (nums.indexOf(val) < 0) return nums.length;

  var count = 0;
  for (var i = 0; i < nums.length; i++) {
    if (nums[i] != val) {
      nums[count++] = nums[i];
    }
  }

  console.log(nums)
  return count;
};


// console.log(removeElement([3, 2, 2, 3], 3))
console.log(removeElement([1], 1))