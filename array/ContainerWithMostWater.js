/**
 * @param {number[]} height
 * @return {number}
 */
var maxArea = function (height) {

  let beginIndex = 0;
  let endIndex = height.length - 1;
  let maxContainArea = 0;

  while(endIndex > beginIndex) {
    const h = Math.min(height[beginIndex], height[endIndex]);
    const w = endIndex - beginIndex
    maxContainArea = Math.max(maxContainArea, h * w);

    if(height[beginIndex] < height[endIndex]) {
      beginIndex += 1;
    } else {
      endIndex -= 1;
    }
  }

  return maxContainArea
};