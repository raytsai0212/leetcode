var thirdMax = function (nums) {
  let tempArr = nums.filter((n, i, arr) => arr.indexOf(n) === i);
  tempArr.sort((a, b) => b - a)

  console.log('tempArr', tempArr[2] || tempArr[0])

  return tempArr[2] ?? tempArr[0]
};


console.log(thirdMax([3, 3, 4, 3, 4, 3, 0, 3, 3]))

