var threeSum = function (nums) {

  nums.sort((a, b) => a - b);
  const result = []

  for (let i = 0; i < nums.length - 2; i += 1) {

    // 如果當前 idx 與前一個相同，則跳過此次回圈
    if (i > 0 && nums[i] === nums[i - 1]) {
      continue;
    }

    let leftIdx = i + 1;
    let rightIdx = nums.length - 1;

    while (rightIdx > leftIdx) {
      const sum = nums[i] + nums[leftIdx] + nums[rightIdx];
      console.log('sum', sum)

      if (sum === 0) {
        result.push([nums[i], nums[leftIdx], nums[rightIdx]]);
        while (nums[leftIdx] === nums[leftIdx + 1]) leftIdx++;
        while (nums[rightIdx] === nums[rightIdx - 1]) rightIdx--;
        leftIdx += 1;
        rightIdx -= 1;
      } else if (sum > 0) {
        rightIdx -= 1;
      } else {
        leftIdx += 1;
      }
  
    }
  }

  return result
};


// var threeSum = function (nums) {
//   nums.sort((a, b) => a - b);
//   let result = [];

//   for (let i = 0; i < nums.length - 2; i++) {
//     if (i > 0 && nums[i] === nums[i - 1]) continue;

//     let left = i + 1, right = nums.length - 1;

//     while (left < right) {

//       let sum = nums[i] + nums[left] + nums[right];
//       console.log(sum)

//       if (sum === 0) {
//         result.push([nums[i], nums[left], nums[right]]);
//         while (nums[left] === nums[left + 1]) left++;
//         while (nums[right] === nums[right - 1]) right--;
//         left++;
//         right--;
//       } else if (sum < 0) {
//         left++;
//       } else {
//         right--;
//       }
//     }
//   }

//   return result;
// };

console.log(threeSum([-1, 0, 1, 2, -1, -4]))