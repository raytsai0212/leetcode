/**
 * @param {string} s
 * @param {number} numRows
 * @return {string}
 */
var convert = function (s, numRows) {
  if (s.length < numRows) {
      return s;
  }

  if (numRows === 1) {
      return s;
  }

  let recordArr = Array(numRows).fill('');
  let direction = false;
  let recordIdx = 0;

  for (let i = 0; i < s.length; i += 1) {
      const curr = s[i]
      recordArr[recordIdx] += curr;

      if(recordIdx === 0 || recordIdx === (numRows -1)){
          direction = !direction;
      }

      recordIdx = direction ? ++recordIdx : --recordIdx
  }

  return recordArr.join('')
};

console.log(convert("PAYPALISHIRING", 3))