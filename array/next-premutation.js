/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var nextPermutation = function (nums) {
  const len = nums.length;
  
  // Step1. 從陣列尾端開始尋找第一個，後項(i+1) > 前項(i) 的 index 位置
  for (let i = len - 2; i >= 0; i -= 1) {
    if (nums[i] < nums[i + 1]) {
        // Step2. 從陣列尾端開始尋找第一個大於 num[index] 的數字
          for (let j = len - 1; j > i; j--) {
              if (nums[j] > nums[i]) {

                // Step3. 找到後，將這兩個位置交換，
                  swap(nums, i, j);
                  // Step 4. 超過 num[index] 的部分，全部反轉，因為原先陣列是降冪，反轉後才可以得到剛好大於的下一個 prematutation
                  reverse(nums, i + 1, len - 1)

                  return
              }
          }
      }
  }

  // 如果沒有下一個 prematutation，反轉全部 // 題目要求的
  nums.reverse();
};

function swap(nums, i, j) {
  const n = nums[i];
  nums[i] = nums[j];
  nums[j] = n;
}

function reverse(nums, start, end) {
  while (start < end) {
      swap(nums, start, end)
      start++;
      end--;
  }
}