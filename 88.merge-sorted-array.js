/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function (nums1, m, nums2, n) {
  if (n === 0) {
      return;
  }

  let idx = 0;
  for (let i = m; i < nums1.length; i++) {
      nums1[i] = nums2[idx];
      idx++;
  }

  nums1.sort((x, y) => x - y);
};

console.log(merge([1, 2, 3, 0, 0, 0], 3, [2, 5, 6], 3));
console.log(merge([0], 0, [1], 1));