/**
 * @param {number} x
 * @return {boolean}
 */
var isPalindrome = function (x) {
  const xStr = String(x);

  if (x < 0) {
    return false;
  }

  if (xStr.length <= 1) {
    return true;
  }

  let endIdx = xStr.length - 1;

  for (let i = 0; i < Math.floor(xStr.length / 2); i++) {

    if (endIdx >= i) {
      if (xStr[i] !== xStr[endIdx]) {
        console.log(2)
        return false;
      }
    }

    endIdx--;
  }

  return true;
};


// console.log(isPalindrome(121))
// console.log(isPalindrome(-121))
// console.log(isPalindrome(10))
// console.log(isPalindrome(0))
console.log(isPalindrome(1001))