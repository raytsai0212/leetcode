/**
 * @param {string} s
 * @return {number}
 */
var romanToInt = function (s) {
  const hashTable = {
    I: 1,
    V: 5,
    X: 10,
    L: 50,
    C: 100,
    D: 500,
    M: 1000,

    IV: 4,
    IX: 9,
    XL: 40,
    XC: 90,
    CD: 400,
    CM: 900
  }

  const extraSigns = [
    'IV',
    'IX',
    'XL',
    'XC',
    'CD',
    'CM',
  ]

  let sum = 0;
  for (let i = 0; i < s.length; i++) {

    const extraSign = `${s[i]}${s[i + 1]}`
    const isExtraSign = extraSigns.includes(extraSign);

    if (isExtraSign) {
      sum += hashTable[extraSign];
      i++;
    } else {
      sum += hashTable[s[i]];
    }
  }

  return sum;
};


/**
 * @param {string} s
 * @return {number}
 */
var romanToInt2 = function (s) {
  const hashTable = {
    I: 1,
    V: 5,
    X: 10,
    L: 50,
    C: 100,
    D: 500,
    M: 1000,
  }
  let sum = 0;
  for (let i = 0; i < s.length; i++) {
    const curr = hashTable[s[i]];
    const next = hashTable[s[i + 1]];


    if (next > curr) {
      sum += next - curr;
      i++;
    } else {
      sum += curr;
    }
  }

  return sum;
};


// console.log(romanToInt('III'));
// console.log(romanToInt('LVIII'));
// console.log(romanToInt('MCMXCIV'));

console.log(romanToInt2('III'));
console.log(romanToInt2('LVIII'));
console.log(romanToInt2('MCMXCIV'));