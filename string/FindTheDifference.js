// solution1
var findTheDifference = function (s, t) {
  if (s.length === 0) return t;

  let str = t;
  for (let i = 0; i < s.length; i++) {
    str = str.replace(s[i], '');
  }

  return str;
};

// solution2
var findTheDifference = function (s, t) {
  if (s.length === 0) return t;

  const obj = {};

  for (let i = 0; i < s.length; i++) {
    if (!obj[s[i]]) {
      obj[s[i]] = 0;
    }
    obj[s[i]]++
  }

  console.log(obj)

  for (let i = 0; i < t.length; i++) {
    if (obj[t[i]]) {
      obj[t[i]]--
    } else {
      return t[i]
    }
  }

  return ''
};

// solution3
var findTheDifference = function (s, t) {
  if (s.length === 0) return t;

  let sSplit = s.split('').sort();
  let tSplit = t.split('').sort();

  for (let i = 0; i < t.length; i++) {
    if (tSplit[i] !== sSplit[i]) {
      return tSplit[i]
    }
  }
};

console.log(findTheDifference2('a', 'aa'))