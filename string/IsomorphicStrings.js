// var isIsomorphic = function (s, t) {
//   if (s.length !== t.length) return false;

//   const charMapping = {};

//   for (let i = 0; i < s.length; i++) {
//     if (!charMapping[s[i]]) {
//       charMapping[s[i]] = t[i];
//     } else {
//       if (charMapping[s[i]] !== t[i]) {
//         return false;
//       }
//     }

//     console.log('charMapping', charMapping)
//   }

//   return true;
// };

var isIsomorphic = function (s, t) {
  const charSMapping = {};
  const charTMapping = {};

  for (let i = 0; i < s.length; i++) {
    if (!charSMapping[s[i]]) {
      charSMapping[s[i]] = t[i];
    } else {
      if (charSMapping[s[i]] !== t[i]) {
        return false;
      }
    }
  }

  for (let i = 0; i < t.length; i++) {
    if (!charTMapping[t[i]]) {
      charTMapping[t[i]] = s[i];
    } else {
      if (charTMapping[t[i]] !== s[i]) {
        return false;
      }
    }
  }


  return true;
};

console.log(isIsomorphic('paper', 'title'))
console.log(isIsomorphic('foo', 'bar'))
console.log(isIsomorphic('badc', 'baba'))