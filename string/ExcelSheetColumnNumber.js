var titleToNumber = function (columnTitle) {
  let sum = 0;
  for (let i = 0; i < columnTitle.length; i++) {
    const v = columnTitle.charCodeAt(i) - 64;
    const n = v * Math.pow(26, columnTitle.length - i - 1);
    sum += n
  }

  return sum
};

console.log(titleToNumber('A'))
console.log(titleToNumber('AB'))
console.log(titleToNumber("FXSHRXW"))