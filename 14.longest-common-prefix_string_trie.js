/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function (strs) {
  strs.sort();
  const beginStr = strs[0];
  const lastStr = strs[strs.length - 1];
  let prefix = '';


  for(let i = 0 ; i < beginStr.length; i++) {
    if(beginStr[i] === lastStr[i]) {
      prefix += beginStr[i];
    } else {
      return prefix;
    }
  }

  return prefix;
};



console.log(longestCommonPrefix(["flower", "flow", "flight"]));
console.log(longestCommonPrefix(["cir", "car"]));
console.log(longestCommonPrefix([""]));
