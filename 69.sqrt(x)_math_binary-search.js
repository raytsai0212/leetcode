/**
 * @param {number} x
 * @return {number}
 */
var mySqrt = function (x) {
  let start = 0;
  while (start * start <= x) {
      start++;
  }

  return start - 1;
};