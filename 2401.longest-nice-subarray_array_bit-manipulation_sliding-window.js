/**
 * @param {number[]} nums
 * @return {number}
 */
var longestNiceSubarray = function (nums) {
  const arr = [];
  let maxLen = 0;

  for (let i = 0; i < nums.length; i++) {

    // arr.push(nums[i]);
    while (!isAndBitwiseEqualToZero(arr, nums[i])) {
      arr.shift();
    }
    
    arr.push(nums[i]);
    maxLen = Math.max(maxLen, arr.length);
    
  };

  console.log('arr -->', arr)
  return arr.length;
}

var isAndBitwiseEqualToZero = (list, nextEle) => {
  const arr = [...list, nextEle]
// console.log('arr -->', arr)
  for (let i = 0; i < arr.length; i++) {
    for (let j = i; j < arr.length; j++) {
      if (i !== j) {
        // console.log('1.',i, arr[i], '2.', j, arr[j], 'r', arr[i] & arr[j])
        const result = arr[i] & arr[j]

        if (result !== 0) {
          console.log(arr, 'false')
          return false;
        }
      }
    }
  }

  console.log(arr, 'true')
  return true;
}

// console.log(longestNiceSubarray([1,3 ]))
console.log(longestNiceSubarray([1,3,8,48,10]))
// console.log(longestNiceSubarray([3,1,5,11,13]))

// console.log(isAndBitwiseEqualToZero([1,2,3,4]))
