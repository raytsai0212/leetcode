var minDepth = (root) => {
  if (!root) return 0;


  const minDepth = (node) => {
    if (node.left === null && node.right === null) {
      return 1;
    }

    if (node.left === null) {
      return minDepth(node.right) + 1;
    }

    if (node.right === null)
      return minDepth(node.left) + 1;


    return Math.min(minDepth(node.left), minDepth(node.right)) + 1
  }

  return minDepth(root)
}