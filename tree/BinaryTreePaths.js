var binaryTreePaths = function (root) {

  if (!root) return;
  let list = [];

  const findPath = (node, str) => {
    if (node.left === null && node.right === null) {
      list.push(str + node.val)
    } else {
      if (node.left !== null) {
        findPath(node.left, str + node.val + '->')
      }

      if (node.right !== null) {
        findPath(node.right, str + node.val + '->')
      }
    }
  }


  findPath(root, '');

  return list;
}