/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
  // 建立一個字典用於記錄 index
  const hashTable = {};

  for(let i = 0; i < nums.length; i++) {
    const remainNum = target - nums[i];

    if(String(remainNum) in hashTable) {
      return [hashTable[String(remainNum)], i];
    }

    hashTable[nums[i]] = i;
  }
};

console.log(twoSum([2,7,11,15], 9));